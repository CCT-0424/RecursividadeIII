#include <iostream>

using namespace std;

int soma(int a, int b){
    if (a == b){
        return a;
    } else {
        if ((b % 2) == 0)
            return -b + soma(a,b-1);
        else
            return b + soma(a,b-1);
    }
}

int main()
{
    cout << soma(1,8) << endl;
    return 0;
}
